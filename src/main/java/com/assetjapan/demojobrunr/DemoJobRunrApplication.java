package com.assetjapan.demojobrunr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@Import(MainConfiguration.class)
@SpringBootApplication
public class DemoJobRunrApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoJobRunrApplication.class, args);
    }

}
